package com.zdeno.rest;

import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import com.zdeno.dao.AnomalyReadingDao;
import com.zdeno.dao.IncidentDao;
import com.zdeno.experimental.postgres.ExportCsv;
import com.zdeno.model.cache.AnomalyReadingEntity;
import com.zdeno.service.*;

@Singleton
@Path("/")
public class App {

    @Inject
    private IncidentDao incidentDao;

    @Inject
    private Logger logger;

    @Inject
    private ExportCsv exportCsv;

    @Inject
    private AnomalyReadingDao anomalyReadingDao;

    private List<Incident> incidents = new ArrayList<>();

    @GET
    @Path("generate/{incident}/{event}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response generate(@PathParam("incident") int number, @PathParam("event") int event) {
        for (int i = 0; i < number; i++) {
            Incident incident = new Incident();
            incident.setName(String.valueOf(UUID.randomUUID().toString()));
            incident.setConfidence(0.8);
            incident.setCreationTime(System.currentTimeMillis());
            incident.setCloseTime(System.currentTimeMillis());
            incident.setLastUpdatedTime(System.currentTimeMillis());
            incident.addDataSource("FMAlarm");
            incident.setOccurrenceFactor(0.5);
            incident.setPriority(5.9);
            Map<String, Long> serviceImpactIndicators = new HashMap<>();
            serviceImpactIndicators.put("ABC", 55L);
            incident.setServiceImpactIndicators(serviceImpactIndicators);
            incident.setStatus(IncidentStatusEnum.CLOSED);

            DimensionKey key = new DimensionKey();
            key.setDimension(DimensionEnum.valueOf("TOPOLOGY"));
            final String hash = UUID.randomUUID().toString();
            key.setDisplayLabel(hash);
            key.setHash(hash);
            key.setParentId(UUID.randomUUID().toString());

            DimensionKey key1 = new DimensionKey();
            key1.setDimension(DimensionEnum.valueOf("SPECIFIC_PROBLEM"));
            final String hash1 = UUID.randomUUID().toString();
            key1.setDisplayLabel(hash1);
            key1.setHash(hash1);

            List<EventWrapper> eventWrappers = new ArrayList<>();
            for (int j = 0; j < event; j++) {
                final EventWrapper<Map<String, Object>> imEvent = new EventWrapper<>();
                imEvent.addDimensionKey(DimensionEnum.valueOf("SPECIFIC_PROBLEM"), key1);
                imEvent.addDimensionKey(DimensionEnum.valueOf("TOPOLOGY"), key);
                imEvent.setDisplayName("Accessibility-FailedActiveUeDLSumPerSec" + j);
                imEvent.setId("Accessibility-FailedActiveUeDLSumPerSecAA" + j);
                imEvent.setSecondaryId("Accessibility-FailedActiveUeDLSumPerSecAA ON X");
                imEvent.setShouldBeClosedAfterNesting(true);
                imEvent.setStatus(Status.CLOSED);
                imEvent.setTopology("LTE01ERBS" + j);
                imEvent.setTimestamp(System.nanoTime());
                imEvent.setSourceDisplayName("ABC" + i);
                Map<String, Object> m1 = new HashMap<>();
                m1.put("type", "SPECIFIC_PROBLEM");
                imEvent.setRawEvent(m1);
                eventWrappers.add(imEvent);
            }
            incident.setEvents(eventWrappers);
            incidents.add(incident);
        }

        long start = System.currentTimeMillis();
        for (Incident incident : incidents) {
            incidentDao.saveIncident(incident);
        }
        logger.info("SAVE {} incidents with {} events took {}ms", number, event, System.currentTimeMillis() - start);
        //        return Response.status(Response.Status.OK).entity(get().getEntity()).build();
        return Response.status(Response.Status.OK)
                .entity("SAVE " + number + " incidents with " + event + " events took " + (System.currentTimeMillis() - start) + "ms").build();
    }

    @GET
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update() {
        long start = System.currentTimeMillis();
        for (final Incident incident : incidents) {

            DimensionKey key1 = new DimensionKey();
            key1.setDimension(DimensionEnum.valueOf("SPECIFIC_PROBLEM"));
            final String hash1 = UUID.randomUUID().toString();
            key1.setDisplayLabel(hash1);
            key1.setHash(hash1);

            final EventWrapper<Map<String, Object>> imEvent = new EventWrapper<>();
            imEvent.addDimensionKey(DimensionEnum.valueOf("SPECIFIC_PROBLEM"), key1);
            imEvent.setDisplayName("Accessibility-FailedActiveUeDLSumPerSec" + incident.getEvents().size());
            imEvent.setId("Accessibility-FailedActiveUeDLSumPerSecAA" + incident.getEvents().size());
            imEvent.setSecondaryId("Accessibility-FailedActiveUeDLSumPerSecAA ON X");
            imEvent.setShouldBeClosedAfterNesting(true);
            imEvent.setStatus(Status.CLOSED);
            imEvent.setTopology("LTE01ERBS" + incident.getEvents().size());
            imEvent.setTimestamp(System.currentTimeMillis());
            imEvent.setSourceDisplayName("ABC" + incident.getEvents().size());
            Map<String, Object> m1 = new HashMap<>();
            m1.put("type", "SPECIFIC_PROBLEM");
            imEvent.setRawEvent(m1);

            List<EventWrapper> eventWrappers = new ArrayList<>();
            eventWrappers.add(imEvent);

            incident.addEventsToIncident(eventWrappers);

            incidentDao.updateIncident(incident);
        }
        logger.info("Update {} incidents took {}ms", incidents.size(), System.currentTimeMillis() - start);
        //        return Response.status(Response.Status.OK).entity(get().getEntity()).build();
        return Response.status(Response.Status.OK)
                .entity("Update " + incidents.size() + " incidents took " + (System.currentTimeMillis() - start) + "ms").build();
    }

    @GET
    @Path("/updateBatch/{number}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBatch(@PathParam("number") int number) {
        //        final List<Incident> incidents = incidentDao.findAllIncidentsWithStatus(IncidentStatusEnum.CLOSED);
        for (final Incident incident : incidents) {

            DimensionKey key1 = new DimensionKey();
            key1.setDimension(DimensionEnum.valueOf("SPECIFIC_PROBLEM"));
            final String hash1 = UUID.randomUUID().toString();
            key1.setDisplayLabel(hash1);
            key1.setHash(hash1);

            List<EventWrapper> eventWrappers = new ArrayList<>();
            for (int j = 0; j < number; j++) {
                final EventWrapper<Map<String, Object>> imEvent = new EventWrapper<>();
                imEvent.addDimensionKey(DimensionEnum.valueOf("SPECIFIC_PROBLEM"), key1);
                imEvent.setDisplayName("Accessibility-FailedActiveUeDLSumPerSec" + j);
                imEvent.setId("Accessibility-FailedActiveUeDLSumPerSecAA" + j);
                imEvent.setSecondaryId("Accessibility-FailedActiveUeDLSumPerSecAA ON X");
                imEvent.setShouldBeClosedAfterNesting(true);
                imEvent.setStatus(Status.CLOSED);
                imEvent.setTopology("LTE01ERBS" + j);
                imEvent.setTimestamp(System.currentTimeMillis());
                imEvent.setSourceDisplayName("ABC" + j);
                Map<String, Object> m1 = new HashMap<>();
                m1.put("type", "SPECIFIC_PROBLEM");
                imEvent.setRawEvent(m1);
                eventWrappers.add(imEvent);
            }
            incident.addEventsToIncident(eventWrappers);
        }
        long start = System.currentTimeMillis();
        incidentDao.batchUpdateIncidents(incidents);
        logger.info("BATCH Update {} incidents took {}ms", incidents.size(), System.currentTimeMillis() - start);
        //        return Response.status(Response.Status.OK).entity(get().getEntity()).build();
        return Response.status(Response.Status.OK)
                .entity("BATCH Update " + incidents.size() + " incidents took " + (System.currentTimeMillis() - start) + "ms").build();

    }

    @GET
    @Path("/xxxx/")
    @Produces(MediaType.APPLICATION_JSON)
    public void xxxx() {
        for (Incident incident : incidents) {
            for (final EventWrapper event : incident.getEvents()) {
                event.setDisplayName(UUID.randomUUID().toString());
            }
        }
        incidentDao.batchUpdateIncidents(incidents);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {
        long start = System.currentTimeMillis();
        final List<Incident> incidents = incidentDao.findAllIncidentsWithStatus(IncidentStatusEnum.CLOSED);
        final Response build = Response.status(Response.Status.OK).entity(incidents).build();
        logger.info("GET {} incidents took {}ms", this.incidents.size(), System.currentTimeMillis() - start);

        //        return build;
        return Response.status(Response.Status.OK)
                .entity("GET " + incidents.size() + " incidents took " + (System.currentTimeMillis() - start) + "ms").build();
    }

    @GET
    @Path("/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete() {
        long start = System.currentTimeMillis();
        final int removed = incidentDao.removeClosedIncidentsOlderThan(TimeUnit.SECONDS, 1);
        //final Response build = Response.status(Response.Status.OK).entity(removed).build();
        logger.info("DELETE {} incidents took {}ms", removed, System.currentTimeMillis() - start);
        incidents.clear();
        return Response.status(Response.Status.OK).entity("DELETE " + removed + " incidents took " + (System.currentTimeMillis() - start) + "ms")
                .build();
        //return build;
    }

    @GET
    @Path("/exportToFile")
    @Produces(MediaType.APPLICATION_JSON)
    public Response exportToFile() {
        return Response.status(Response.Status.OK).entity(exportCsv.exportToFile()).build();
    }

    @GET
    @Path("/readPsqlInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readPsqlInfo() {
        return Response.status(Response.Status.OK).entity(exportCsv.readPsqlInfo()).build();
    }

    @GET
    @Path("/anomalyReading/save/{count}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveAnomalyReading(@PathParam("count") int number) {
        List<AnomalyReadingEntity> list = new ArrayList<>(number);
        for (int i = 0; i < number; i++) {
            final AnomalyReadingEntity anomalyReadingEntity = new AnomalyReadingEntity();
            anomalyReadingEntity.setCell(UUID.randomUUID().toString());
            anomalyReadingEntity.setKpiName(UUID.randomUUID().toString());
            anomalyReadingEntity.setTimeStamp(i);
            anomalyReadingEntity.setLowerBoundary(i);
            anomalyReadingEntity.setUpperBoundary(i);
            anomalyReadingEntity.setValue(i);
            list.add(anomalyReadingEntity);
        }
        anomalyReadingDao.save(list);
        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Path("/anomalyReading/save/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveAnomalyReadingsForFullNetwork() {
        List<String> nodes = new ArrayList<>(5000);
        for (int i = 0; i < 5000; i++) {
            nodes.add("LTE" + i);
        }
        List<String> cells = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            cells.add("-" + i);
        }
        List<String> kpis = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            kpis.add("Kpi" + i);
        }
        List<AnomalyReadingEntity> list = new ArrayList<>(2_400_000);
        int count = 0;
        for (int i = 0; i < 4 * 24; i++) {
            for (final String cell : cells) {
                for (final String node : nodes) {
                    final AnomalyReadingEntity anomalyReadingEntity = new AnomalyReadingEntity();
                    anomalyReadingEntity.setCell(node + cell);
                    anomalyReadingEntity.setKpiName(kpis.get(count));
                    anomalyReadingEntity.setTimeStamp(i);
                    anomalyReadingEntity.setLowerBoundary(i);
                    anomalyReadingEntity.setUpperBoundary(i);
                    anomalyReadingEntity.setValue(i);
                    list.add(anomalyReadingEntity);
                    if (++count % kpis.size() == 0) {
                        count = 0;
                    }
                }
            }
        }
        anomalyReadingDao.save(list);
        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Path("/anomalyReading/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAnomaluReadings() {
        anomalyReadingDao.deleteAllBetween(0, 10_000_000);
        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Path("/anomalyReading/get/{kpi}/{cell}/{start}/{end}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAnomalyReadings(@PathParam("kpi") String kpi, @PathParam("cell") String cell, @PathParam("start") long start,
                                       @PathParam("end") long end) {
        return Response.status(Response.Status.OK).entity(anomalyReadingDao.getQuery(cell, kpi, start, end)).build();
    }

    @GET
    @Path("/anomalyReading/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAnomalyReadings() {
        return Response.status(Response.Status.OK).entity(anomalyReadingDao.getAll()).build();
    }
}
