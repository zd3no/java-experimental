package com.zdeno.model;

import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Parameter;

import com.zdeno.service.IncidentStatusEnum;

//@Entity
//@Table(name = "incident_aim")
//@Cacheable
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//public class AimIncident {
//
//    @Id
//    @Column(name = "id", unique = true)
//    @GeneratedValue(
//        strategy = GenerationType.SEQUENCE,
//        generator = "incident_generator"
//    )
//    @GenericGenerator(
//        name = "incident_generator",
//        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
//        parameters = {
//            @Parameter(name = "sequence_name", value = "incident_sequence"),
//            @Parameter(name = "initial_value", value = "1"),
//            @Parameter(name = "increment_size", value = "5"),
//            @Parameter(name = "optimizer", value = "pooled-lo")
//        }
//    )
//    private long id;
//    @Column(name = "name", columnDefinition = "text")
//    private String name;
//    @Column(name = "creation_time")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date creationTime;
//    @Column(name = "expired_time")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date closeTime;
//    @Column(name = "last_updated_time")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date lastUpdatedTime;
//    @Column(name = "confidence")
//    private double confidence;
//    @Column(name = "occurrence_factor")
//    private double occurrenceFactor;
//    /**
//     * Inside the application is a Set<String>
//     */
//    @Column(name = "data_source", columnDefinition = "text")
//    private String dataSources;
//
//    /**
//     * Values from IncidentStatusEnum
//     */
//    @Column(name = "status")
//    private String status;
//
//    /**
//     * Inside the application is a Set<String>
//     */
//    @Column(name = "dimension_keys", columnDefinition = "text")
//    private String dimensionKeys;
//
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @OneToMany(mappedBy = "incident", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
//    private List<AimEvent> events;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(final long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(final String name) {
//        this.name = name;
//    }
//
//    public Date getCreationTime() {
//        return creationTime;
//    }
//
//    public void setCreationTime(final Date creationTime) {
//        this.creationTime = creationTime;
//    }
//
//    public Date getCloseTime() {
//        return closeTime;
//    }
//
//    public void setCloseTime(final Date closeTime) {
//        this.closeTime = closeTime;
//    }
//
//    public Date getLastUpdatedTime() {
//        return lastUpdatedTime;
//    }
//
//    public void setLastUpdatedTime(final Date lastUpdatedTime) {
//        this.lastUpdatedTime = lastUpdatedTime;
//    }
//
//    public double getConfidence() {
//        return confidence;
//    }
//
//    public void setConfidence(final double confidence) {
//        this.confidence = confidence;
//    }
//
//    public double getOccurrenceFactor() {
//        return occurrenceFactor;
//    }
//
//    public void setOccurrenceFactor(final double occurrenceFactor) {
//        this.occurrenceFactor = occurrenceFactor;
//    }
//
//    public String getDataSources() {
//        return dataSources;
//    }
//
//    public void setDataSources(final String dataSources) {
//        this.dataSources = dataSources;
//    }
//
//    public String getStatus() {
//        if (status == null) {
//            return IncidentStatusEnum.CLOSED.toString();
//        }
//        return status;
//    }
//
//    public void setStatus(final String status) {
//        this.status = status;
//    }
//
//    public String getDimensionKeys() {
//        return dimensionKeys;
//    }
//
//    public void setDimensionKeys(final String dimensionKeys) {
//        this.dimensionKeys = dimensionKeys;
//    }
//
//    public List<AimEvent> getEvents() {
//        if (events == null) {
//            events = new ArrayList<>();
//        }
//        return events;
//    }
//
//    public void addEvents(final List<AimEvent> events) {
//        if (this.events == null) {
//            this.events = new ArrayList<>();
//        }
//        for (final AimEvent event : events) {
//            event.setIncident(this);
//            this.events.add(event);
//        }
//    }
//
//    @Override public boolean equals(final Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (!(o instanceof AimIncident)) {
//            return false;
//        }
//        final AimIncident that = (AimIncident) o;
//        return id == that.id;
//    }
//
//    @Override public int hashCode() {
//        return Objects.hash(id);
//    }
//}
@Entity
@Table(name = "incident_aim")
public class AimIncident {

    @Id
    @Column(name = "id", unique = true)
    private long id;
    @Column(name = "name", columnDefinition = "text")
    private String name;
    @Column(name = "creation_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;
    @Column(name = "expired_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closeTime;
    @Column(name = "last_updated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdatedTime;
    @Column(name = "confidence")
    private double confidence;
    @Column(name = "occurrence_factor")
    private double occurrenceFactor;
    /**
     * Inside the application is a Set<String>
     */
    @Column(name = "data_source", columnDefinition = "text")
    private String dataSources;

    /**
     * Values from IncidentStatusEnum
     */
    @Column(name = "status")
    private String status;

    /**
     * Inside the application is a Set<String>
     */
    @Column(name = "dimension_keys", columnDefinition = "text")
    private String dimensionKeys;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "incident", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<AimEvent> events;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(final Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(final Date closeTime) {
        this.closeTime = closeTime;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(final Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(final double confidence) {
        this.confidence = confidence;
    }

    public double getOccurrenceFactor() {
        return occurrenceFactor;
    }

    public void setOccurrenceFactor(final double occurrenceFactor) {
        this.occurrenceFactor = occurrenceFactor;
    }

    public String getDataSources() {
        return dataSources;
    }

    public void setDataSources(final String dataSources) {
        this.dataSources = dataSources;
    }

    public String getStatus() {
        if (status == null) {
            return IncidentStatusEnum.CLOSED.toString();
        }
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getDimensionKeys() {
        return dimensionKeys;
    }

    public void setDimensionKeys(final String dimensionKeys) {
        this.dimensionKeys = dimensionKeys;
    }

    public List<AimEvent> getEvents() {
        if (events == null) {
            events = new ArrayList<>();
        }
        return events;
    }

    public void addEvents(final List<AimEvent> events) {
        if (this.events == null) {
            this.events = new ArrayList<>();
        }
        for (final AimEvent event : events) {
            event.setIncident(this);
            this.events.add(event);
        }
    }
}
