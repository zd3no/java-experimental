package com.zdeno.model;

//@Entity
//@Table(name = "event")
//@Cacheable
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//public class AimEvent {
//
//    /**
//     * PK
//     */
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_generator")
//    @GenericGenerator(name = "event_generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
//            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "incident_sequence"),
//            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
//            @org.hibernate.annotations.Parameter(name = "increment_size", value = "5"),
//            @org.hibernate.annotations.Parameter(name = "optimizer", value = "pooled-lo") })
//    @Column(name = "identifier", unique = true)
//    private long identifier;
//
//    /**
//     * A unique id for this event
//     */
//    @Column(name = "id")
//    private String id;
//    /**
//     * A user friendly display name for the event itself. could be based on anything...
//     */
//    @Column(name = "display_name")
//    private String displayName;
//    /**
//     * A user friendly display name for the source for this event like FMAlarm, Configuration Change, Command
//     */
//    @Column(name = "source_display_name")
//    private String sourceDisplayName;
//    /**
//     * where this event is coming from to targeted at. note this could be a Network node or cell but also a geo location or function type depending on
//     * the feed
//     */
//    @Column(name = "topology")
//    private String topology;
//    @Column(name = "status")
//    private String status;
//    /**
//     * Data used to merge another attributes
//     */
//    @Column(name = "secondary_id")
//    private String secondaryId;
//    @Column(name = "last_update")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date timestamp;
//    /**
//     * Inside the application is a Set<String>
//     */
//    @Column(name = "dimension_keys", columnDefinition = "text")
//    private String dimensionKeys;
//
//    @Column(name = "raw", columnDefinition = "text")
//    private String rawEvent;
//
//    @ManyToOne
//    @JoinColumn(name = "incident_id", nullable = false)
//    private AimIncident incident;
//
//    public long getIdentifier() {
//        return identifier;
//    }
//
//    public void setIdentifier(final long identifier) {
//        this.identifier = identifier;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(final String id) {
//        this.id = id;
//    }
//
//    public String getDisplayName() {
//        return displayName;
//    }
//
//    public void setDisplayName(final String displayName) {
//        this.displayName = displayName;
//    }
//
//    public String getSourceDisplayName() {
//        return sourceDisplayName;
//    }
//
//    public void setSourceDisplayName(final String sourceDisplayName) {
//        this.sourceDisplayName = sourceDisplayName;
//    }
//
//    public String getTopology() {
//        return topology;
//    }
//
//    public void setTopology(final String topology) {
//        this.topology = topology;
//    }
//
//    public String getStatus() {
//        if (status == null) {
//            return Status.NONE.toString();
//        }
//        return status;
//    }
//
//    public void setStatus(String status) {
//        if (Status.valueOf(status) == null) {
//            status = Status.NONE.toString();
//        }
//        this.status = status;
//    }
//
//    public String getSecondaryId() {
//        return secondaryId;
//    }
//
//    public void setSecondaryId(final String secondaryId) {
//        this.secondaryId = secondaryId;
//    }
//
//    public Date getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(final Date timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public AimIncident getIncident() {
//        return incident;
//    }
//
//    public void setIncident(final AimIncident incident) {
//        this.incident = incident;
//    }
//
//    public String getDimensionKeys() {
//        return dimensionKeys;
//    }
//
//    public void setDimensionKeys(final String dimensionKeys) {
//        this.dimensionKeys = dimensionKeys;
//    }
//
//    public String getRawEvent() {
//        return rawEvent;
//    }
//
//    public void setRawEvent(final String rawEvent) {
//        this.rawEvent = rawEvent;
//    }
//
//    @Override
//    public boolean equals(final Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (!(o instanceof AimEvent)) {
//            return false;
//        }
//        final AimEvent aimEvent = (AimEvent) o;
//        return identifier == aimEvent.identifier && Objects.equals(incident, aimEvent.incident);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(identifier, incident);
//    }
//}

import java.util.Date;
import java.util.Objects;

import javax.persistence.*;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.zdeno.service.Status;

@Entity
@Table(name = "event")
public class AimEvent {

    /**
         * PK
         */
        @Id
//        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_generator")
//        @GenericGenerator(name = "event_generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
//                @org.hibernate.annotations.Parameter(name = "sequence_name", value = "incident_sequence"),
//                @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
//                @org.hibernate.annotations.Parameter(name = "increment_size", value = "5"),
//                @org.hibernate.annotations.Parameter(name = "optimizer", value = "pooled-lo") })
        @Column(name = "identifier", unique = true)
        private long identifier;

    /**
     * A unique id for this event
     */
    @Column(name = "id")
    private String id;
    /**
     * A user friendly display name for the event itself. could be based on anything...
     */
    @Column(name = "display_name")
    private String displayName;
    /**
     * A user friendly display name for the source for this event like FMAlarm, Configuration Change, Command
     */
    @Column(name = "source_display_name")
    private String sourceDisplayName;
    /**
     * where this event is coming from to targeted at. note this could be a Network node or cell but also a geo location or function type depending on
     * the feed
     */
    @Column(name = "topology")
    private String topology;
    @Column(name = "status")
    private String status;
    /**
     * Data used to merge another attributes
     */
    @Column(name = "secondary_id")
    private String secondaryId;
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    /**
     * Inside the application is a Set<String>
     */
    @Column(name = "dimension_keys", columnDefinition = "text")
    private String dimensionKeys;

    @Column(name = "raw", columnDefinition = "text")
    private String rawEvent;

    @ManyToOne
    @JoinColumn(name = "incident_id", nullable = false)
    private AimIncident incident;

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final long identifier) {
        this.identifier = identifier;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    public String getSourceDisplayName() {
        return sourceDisplayName;
    }

    public void setSourceDisplayName(final String sourceDisplayName) {
        this.sourceDisplayName = sourceDisplayName;
    }

    public String getTopology() {
        return topology;
    }

    public void setTopology(final String topology) {
        this.topology = topology;
    }

    public String getStatus() {
        if (status == null) {
            return Status.NONE.toString();
        }
        return status;
    }

    public void setStatus(String status) {
        if (Status.valueOf(status) == null) {
            status = Status.NONE.toString();
        }
        this.status = status;
    }

    public String getSecondaryId() {
        return secondaryId;
    }

    public void setSecondaryId(final String secondaryId) {
        this.secondaryId = secondaryId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public AimIncident getIncident() {
        return incident;
    }

    public void setIncident(final AimIncident incident) {
        this.incident = incident;
    }

    public String getDimensionKeys() {
        return dimensionKeys;
    }

    public void setDimensionKeys(final String dimensionKeys) {
        this.dimensionKeys = dimensionKeys;
    }

    public String getRawEvent() {
        return rawEvent;
    }

    public void setRawEvent(final String rawEvent) {
        this.rawEvent = rawEvent;
    }
}
