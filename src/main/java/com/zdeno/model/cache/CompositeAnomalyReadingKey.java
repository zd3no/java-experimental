package com.zdeno.model.cache;

import java.io.Serializable;

public class CompositeAnomalyReadingKey implements Serializable{
    String cell;
    String kpiName;
    long timeStamp;
}
