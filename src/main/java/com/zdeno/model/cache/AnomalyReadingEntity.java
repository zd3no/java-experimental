package com.zdeno.model.cache;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity(name = "anomaly_reading")
@IdClass(CompositeAnomalyReadingKey.class)
public class AnomalyReadingEntity implements Serializable{

    @Id
    String cell;
    @Id
    String kpiName;
    @Id
    long timeStamp;
    double value;
    double upperBoundary;
    double lowerBoundary;

    public String getCell() {
        return cell;
    }

    public void setCell(final String cell) {
        this.cell = cell;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(final String kpiName) {
        this.kpiName = kpiName;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(final long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getValue() {
        return value;
    }

    public void setValue(final double value) {
        this.value = value;
    }

    public double getUpperBoundary() {
        return upperBoundary;
    }

    public void setUpperBoundary(final double upperBoundary) {
        this.upperBoundary = upperBoundary;
    }

    public double getLowerBoundary() {
        return lowerBoundary;
    }

    public void setLowerBoundary(final double lowerBoundary) {
        this.lowerBoundary = lowerBoundary;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnomalyReadingEntity)) {
            return false;
        }
        final AnomalyReadingEntity that = (AnomalyReadingEntity) obj;
        return timeStamp == that.timeStamp && Double.compare(that.value, value) == 0 && Double.compare(that.upperBoundary, upperBoundary) == 0
                && Double.compare(that.lowerBoundary, lowerBoundary) == 0 && Objects.equals(cell, that.cell) && Objects.equals(kpiName, that.kpiName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cell, kpiName, timeStamp, value, upperBoundary, lowerBoundary);
    }
}
