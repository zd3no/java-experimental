package com.zdeno.dao;

import java.util.List;

import com.zdeno.model.cache.AnomalyReadingEntity;

public interface AnomalyReadingDao {

    void save(List<AnomalyReadingEntity> entities);

    void deleteAllBetween(long start, long end);

    List<AnomalyReadingEntity> getAll();

    List<AnomalyReadingEntity> getQuery(String cell, String kpi, long startTime, long endTime);
}
