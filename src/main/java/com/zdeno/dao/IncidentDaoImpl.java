package com.zdeno.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;

import com.zdeno.model.AimIncident;
import com.zdeno.service.Incident;
import com.zdeno.service.IncidentStatusEnum;
import com.zdeno.service.util.IncidentDBMapper;

/**
 * Handles the persistence of AimIncidents
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class IncidentDaoImpl implements IncidentDao {

    @PersistenceContext(unitName = "aimManager")
    private EntityManager entityManager;

    @Inject
    private IncidentDBMapper incidentDBMapper;

    @Inject
    private Logger logger;

    @SuppressWarnings("unchecked")
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<Incident> findAllIncidentsWithStatus(final IncidentStatusEnum status) {
        logger.info("Retrieving all incidents with status {}", status);
        final List<Incident> incidentsFound = new ArrayList<>();
        final Query query = entityManager
                .createQuery("FROM " + AimIncident.class.getName() + " inc");
        final List<AimIncident> results = (List<AimIncident>) query.getResultList();
        for (final AimIncident aimIncident : results) {
            incidentsFound.add(incidentDBMapper.transformAimIncidentToIncident(aimIncident));
        }
        return incidentsFound;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Incident> findAllIncidentsWithStatusRequired(final IncidentStatusEnum status) {
        logger.info("Retrieving all incidents with status {}", status);
        final List<Incident> incidentsFound = new ArrayList<>();
        final Query query = entityManager
            .createQuery("FROM " + AimIncident.class.getName() + " inc");
        final List<AimIncident> results = (List<AimIncident>) query.getResultList();
        for (final AimIncident aimIncident : results) {
            incidentsFound.add(incidentDBMapper.transformAimIncidentToIncident(aimIncident));
        }
        return incidentsFound;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Incident findIncidentById(final long incidentId) {
        logger.debug("Finding incident with id {}", incidentId);
        AimIncident aimIncident = entityManager.find(AimIncident.class, incidentId);
        if (aimIncident == null) {
            return null;
        }
        return incidentDBMapper.transformAimIncidentToIncident(aimIncident);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int removeClosedIncidentsOlderThan(final TimeUnit timeUnit, final int timeValue) {
        final Date dateToRemove = new Date(new Date().getTime() - timeUnit.toMillis(timeValue));
        logger.info("Deleting Incidents with status CLOSED and lastUpdatedTime older than {} minutes", dateToRemove);
        final Query query = entityManager
                .createQuery("DELETE FROM " + AimIncident.class.getName() + " i WHERE i.status = :status AND i.lastUpdatedTime < :time ");
        query.setParameter("status", IncidentStatusEnum.CLOSED.toString());
        query.setParameter("time", dateToRemove);

        return query.executeUpdate();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void saveIncident(final Incident incident) {
        logger.debug("Saving in DB Incident {}, id {}", incident.getName(), incident.getId());
        if (incident.getId() > 0) {
            logger.warn("Incident cannot be persisted in DB because it already has an id [{}]. Was this incident previously persisted ?",
                    incident.getId());
            return;
        }
        final AimIncident aimIncident = incidentDBMapper.transformIncidentToAimIncident(incident);
        entityManager.persist(aimIncident);
//        entityManager.flush();
        incident.setId(aimIncident.getId());
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateIncident(final Incident incident) {
        logger.debug("Updating in DB Incident {}, id {}", incident.getName(), incident.getId());
        AimIncident aimIncident = incidentDBMapper.transformIncidentToAimIncident(incident);
        aimIncident = entityManager.merge(aimIncident);
        incidentDBMapper.syncEventIdentifiers(aimIncident, incident);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void batchUpdateIncidents(final Iterable<Incident> incidents) {
        logger.debug("Starting incident batch update...");
        int batchSize = 50;
        int count = 0;
        for (final Incident incident : incidents) {
            AimIncident aimIncident = incidentDBMapper.transformIncidentToAimIncident(incident);
            aimIncident = entityManager.merge(aimIncident);
            incidentDBMapper.syncEventIdentifiers(aimIncident, incident);
            count++;
            if (count % batchSize == 0) {
                entityManager.flush();
                entityManager.clear();
            }
        }
        entityManager.flush();
        entityManager.clear();
        logger.debug("Batch updated in DB {} incidents", count);
    }

    @SuppressWarnings("unchecked")
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<Incident> findAllClosedIncidentsWithoutEvents() {
        logger.debug("Finding CLOSED incidents ignoring events...");
        final List<Incident> incidentsFound = new ArrayList<>();
        final Query query = entityManager.createQuery(
                "SELECT i.id, i.name, i.creationTime, i.closeTime, i.lastUpdatedTime, i.confidence, i.occurrenceFactor, i.dataSources, i.status, i.dimensionKeys  FROM "
                        + AimIncident.class.getName() + " AS i WHERE i.status = 'CLOSED' ");
        final List<Object[]> results = (List<Object[]>) query.getResultList();
        for (final Object[] objects : results) {
            incidentsFound.add(incidentDBMapper.transformAimIncidentWithoutEventsToIncident(objects));
        }
        logger.debug("Extracted {} CLOSED incidents. Events were ignored", incidentsFound.size());
        return incidentsFound;
    }
}
