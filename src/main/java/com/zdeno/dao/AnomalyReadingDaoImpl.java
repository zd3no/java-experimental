package com.zdeno.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;

import com.zdeno.model.cache.AnomalyReadingEntity;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class AnomalyReadingDaoImpl implements AnomalyReadingDao {

    @PersistenceContext(unitName = "aimManager")
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public void save(final List<AnomalyReadingEntity> entities) {
        final long start = System.currentTimeMillis();
        logger.info("Saving anomaly readings...");
        int batchSize = 1000;
        int count = 0;
        for (AnomalyReadingEntity entity : entities) {
            entityManager.persist(entity);
            count++;
            if (count % batchSize == 0) {
                entityManager.flush();
                entityManager.clear();
            }
        }
        entityManager.flush();
        entityManager.clear();
        logger.info("Saved {} entities in {} ms", entities.size(), System.currentTimeMillis() - start);
    }

    @Override
    public void deleteAllBetween(final long start, final long end) {
        final long startX = System.currentTimeMillis();
        logger.info("Deleting anomaly readings between {} and {}", start, end);
        final Query query = entityManager.createQuery("DELETE from anomaly_reading as a where a.timeStamp between :startTime and :endTime");
        query.setParameter("startTime", start);
        query.setParameter("endTime", end);
        final int i = query.executeUpdate();
        logger.info("Deleted {} entities in {} ms", i, System.currentTimeMillis() - startX);
    }

    @Override
    public List<AnomalyReadingEntity> getAll() {
        final long start = System.currentTimeMillis();
        logger.info("getting anomaly readings...");
        final Query query = entityManager.createQuery("SELECT a from anomaly_reading as a");
        final List resultList = query.getResultList();
        logger.info("Extracted {} entities in {} ms", resultList.size(), System.currentTimeMillis() - start);
        return resultList;
    }

    @Override
    public List<AnomalyReadingEntity> getQuery(final String cell, final String kpi, final long startTime, final long endTime) {
        final long start = System.currentTimeMillis();
        logger.info("Getting anomaly readings for {} and {}", cell, kpi);
        final Query query = entityManager.createQuery(
                "SELECT a from anomaly_reading as a where a.cell = :cell and a.kpiName = :kpi and a.timeStamp between :startTime and :endTime");
        query.setParameter("cell", cell);
        query.setParameter("kpi", kpi);
        query.setParameter("startTime", startTime);
        query.setParameter("endTime", endTime);
        final List resultList = query.getResultList();
        logger.info("Extracted {} entities in {} ms", resultList.size(), System.currentTimeMillis() - start);
        return resultList;
    }
}
