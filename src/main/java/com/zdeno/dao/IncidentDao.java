package com.zdeno.dao;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.zdeno.service.Incident;
import com.zdeno.service.IncidentStatusEnum;

/**
 * Handles persistence of incidents
 */
public interface IncidentDao {

    /**
     * Gets all the Incidents stored with a specific status
     *
     * @param status
     * @return List of incidents
     */
    List<Incident> findAllIncidentsWithStatus(IncidentStatusEnum status);

    @SuppressWarnings("unchecked") List<Incident> findAllIncidentsWithStatusRequired(IncidentStatusEnum status);

    /**
     * @param id
     * @return incident or null if its not found
     */
    Incident findIncidentById(long id);

    /**
     * Removes from storage all incidents older than a period of time
     *
     * @param timeUnit
     *            like days
     * @param timeValue
     *            like 7
     * @return value of the remove operation: positive =: rows affected, -1 =: statement succeeded, but there is no update count information available
     */
    int removeClosedIncidentsOlderThan(TimeUnit timeUnit, int timeValue);

    /**
     * Saves incident in the persistent storage.
     *
     * @param incident
     */
    void saveIncident(Incident incident);

    /**
     * Update incident in the persistence storage
     *
     * @param incident
     */
    void updateIncident(Incident incident);

    void batchUpdateIncidents(Iterable<Incident> incidents);

    /**
     * Find all incidents where status is closed. Ignore Events.
     *
     * @return incidents or empty list.
     */
    List<Incident> findAllClosedIncidentsWithoutEvents();
}
