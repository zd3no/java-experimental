package com.zdeno.experimental.requiresnew;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.zdeno.dao.IncidentDao;
import com.zdeno.service.IncidentStatusEnum;

@Stateless
public class RequiresNew {

    @Inject IncidentDao incidentDao;


    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int dothis(){
        final ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < 1_000; i++) {
            strings.add(String.valueOf(i));
        }
        return strings.size();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void getIncidentFromDB(){
        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
            incidentDao.findAllIncidentsWithStatus(IncidentStatusEnum.CLOSED);
        }
    }
}
