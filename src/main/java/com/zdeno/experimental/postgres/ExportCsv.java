package com.zdeno.experimental.postgres;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Singleton
public class ExportCsv {

    @PersistenceContext(unitName = "aimManager")
    private EntityManager entityManager;


    public Object readPsqlInfo() {
        final Query query = entityManager.createNativeQuery("\\d event");
        return query.getSingleResult();
    }

    public Object exportToFile() {
        final Query query = entityManager.createNativeQuery("Copy (Select * From incident_aim AS inc, event WHERE inc.id = event.incident_id) To '/tmp/test.csv' With CSV DELIMITER ',';");
        return query.getResultList();
    }

}
