package com.zdeno.experimental.properties;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class PrettyProperties {

    @PostConstruct
    public void postConstruct() {
        final Map<String, String> map = new HashMap<>();
        map.put("Hello", "World");
        map.put("comment", "This is a comment");
        map.put("Another", "World");
        map.put("comment", "This is a comment");
        writeToProperties(map);
    }

    private void writeToProperties(final Map<String, String> configurations) {
        try {
            File file = new File("/tmp/my.properties");
            FileWriter fileWriter = new FileWriter(file);
            for (final Map.Entry<String, String> entry : configurations.entrySet()) {
                String value = entry.getValue();
                if (value == null) {
                    value = "";
                }
                if (entry.getKey().equals("comment")) {
                    fileWriter.write(System.lineSeparator() + "# ========= " + value + " ========= #" + System.lineSeparator());
                } else {
                    fileWriter.write(entry.getKey() + "=" + value + System.lineSeparator());
                }
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
