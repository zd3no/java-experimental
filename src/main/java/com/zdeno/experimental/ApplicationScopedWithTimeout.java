package com.zdeno.experimental;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
@Startup
public class ApplicationScopedWithTimeout {

    @Resource
    private TimerService timerService;

    @Inject
    private Logger logger;

    @PostConstruct
    private void post() {
        logger.info("####### PostConstruct method works in @ApplicationSScoped bean ###########");

        final TimerConfig timerConfig = new TimerConfig("ABC", false);
        timerService.createIntervalTimer(0, 1000, timerConfig);
    }

    @Timeout
    private void timeout() {
        logger.info("####### Timer service and @Timeout method works in @ApplicationSScoped bean ###########");
    }
}
