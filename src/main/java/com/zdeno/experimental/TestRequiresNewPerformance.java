package com.zdeno.experimental;

import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.slf4j.Logger;

import com.zdeno.dao.IncidentDao;
import com.zdeno.experimental.requires.Requires;
import com.zdeno.experimental.requiresnew.RequiresNew;
import com.zdeno.service.*;

@Singleton
@Startup
public class TestRequiresNewPerformance {

    @Inject
    Requires requires;

    @Inject
    RequiresNew requiresNew;

    @Inject
    Logger logger;

    @Inject
    IncidentDao incidentDao;

    @PostConstruct
    public void post() {
        long start = System.currentTimeMillis();
        final int count = 10000;
        for (int i = 0; i < count; i++) {
            final int result = requires.dothis() + i;
        }
        logger.info("############## REQUIRES over {} iterations took {}ms", count, System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            final int result = requiresNew.dothis() + i;
        }
        logger.info("############## REQUIRES_NEW over {} iterations took {}ms", count, System.currentTimeMillis() - start);


        incidentDao.removeClosedIncidentsOlderThan(TimeUnit.MILLISECONDS, 1);

        Incident incident = new Incident();
        incident.setName(String.valueOf(UUID.randomUUID().toString()));
        incident.setConfidence(0.8);
        incident.setCreationTime(System.currentTimeMillis());
        incident.setCloseTime(System.currentTimeMillis());
        incident.setLastUpdatedTime(System.currentTimeMillis());
        incident.addDataSource("FMAlarm");
        incident.setOccurrenceFactor(0.5);
        incident.setPriority(5.9);
        Map<String, Long> serviceImpactIndicators = new HashMap<>();
        serviceImpactIndicators.put("ABC", 55L);
        incident.setServiceImpactIndicators(serviceImpactIndicators);
        incident.setStatus(IncidentStatusEnum.CLOSED);

        DimensionKey key = new DimensionKey();
        key.setDimension(DimensionEnum.valueOf("TOPOLOGY"));
        final String hash = UUID.randomUUID().toString();
        key.setDisplayLabel(hash);
        key.setHash(hash);
        key.setParentId(UUID.randomUUID().toString());

        DimensionKey key1 = new DimensionKey();
        key1.setDimension(DimensionEnum.valueOf("SPECIFIC_PROBLEM"));
        final String hash1 = UUID.randomUUID().toString();
        key1.setDisplayLabel(hash1);
        key1.setHash(hash1);

        List<EventWrapper> eventWrappers = new ArrayList<>();
        for (int j = 0; j < 2; j++) {
            final EventWrapper<Map<String, Object>> imEvent = new EventWrapper<>();
            imEvent.addDimensionKey(DimensionEnum.valueOf("SPECIFIC_PROBLEM"), key1);
            imEvent.addDimensionKey(DimensionEnum.valueOf("TOPOLOGY"), key);
            imEvent.setDisplayName("Accessibility-FailedActiveUeDLSumPerSec" + j);
            imEvent.setId("Accessibility-FailedActiveUeDLSumPerSecAA" + j);
            imEvent.setSecondaryId("Accessibility-FailedActiveUeDLSumPerSecAA ON X");
            imEvent.setShouldBeClosedAfterNesting(true);
            imEvent.setStatus(Status.CLOSED);
            imEvent.setTopology("LTE01ERBS" + j);
            imEvent.setTimestamp(System.nanoTime());
            imEvent.setSourceDisplayName("ABC" + j);
            Map<String, Object> m1 = new HashMap<>();
            m1.put("type", "SPECIFIC_PROBLEM");
            imEvent.setRawEvent(m1);
            eventWrappers.add(imEvent);
        }
        incident.setEvents(eventWrappers);

        incidentDao.saveIncident(incident);
        testWithDbAccess();
    }

    private void testWithDbAccess() {

        long start = System.currentTimeMillis();
        final int count = 50;
        for (int i = 0; i < count; i++) {
            requires.getIncidentFromDB();
        }
        logger.info("############## DB REQUIRED access over {} iterations took {}ms", count, System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            requiresNew.getIncidentFromDB();
        }
        logger.info("############## DB REQUIRES_NEW access over {} iterations took {}ms", count, System.currentTimeMillis() - start);
    }
}
