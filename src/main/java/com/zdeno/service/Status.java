package com.zdeno.service;

/**
 * Status is normalized and the individual states are relevant to incident management rather than the states of the events
 */
public enum Status {
    CLOSED("CLOSED"),
    OPEN("OPEN"),
    NONE("NONE");
    private String name;

    Status(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
