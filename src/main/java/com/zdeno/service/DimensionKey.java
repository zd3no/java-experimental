/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zdeno.service;

import java.io.Serializable;
import java.util.Objects;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author paddy
 */
public class DimensionKey implements Comparable<DimensionKey>, Serializable {
    private static final long serialVersionUID = 4953592922846215231L;
    private static final String TAG_DIVIDER = "|";

    private String displayLabel = "";
    private Object hash = "";
    private DimensionEnum dimension = null;
    private String tag = "other";
    /**
     * parentId should be set to Network Element FDN for topology Dimension, this is used for grouping cells belonging to same FDN in the
     * KnowledgeBase
     */
    private String parentId = "";
    private boolean learningSource = false;

    public String getDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(final String displayLabel) {
        this.displayLabel = displayLabel;
    }

    public Object getHash() {
        return hash;
    }

    public void setHash(final Object hash) {
        this.hash = hash;
    }

    public DimensionEnum getDimension() {
        return dimension;
    }

    public void setDimension(final DimensionEnum dimension) {
        this.dimension = dimension;
    }

    public void setLearningSource(final boolean learningSource) {
        this.learningSource = learningSource;
    }

    public void setParentId(final String parentId) {
        this.parentId = parentId;
    }

    public boolean isLearningSource() {
        return learningSource;
    }

    public String getParentId() {
        return parentId;
    }

    public String getTag() {
        if (tag.contains(TAG_DIVIDER)) {
            return tag.split("\\" + TAG_DIVIDER)[0];
        }
        return tag;
    }

    public void setTag(final String tag) {
        if (!this.tag.contains(TAG_DIVIDER)) {
            this.tag = tag;
        } else {
            this.tag = tag + TAG_DIVIDER + this.getTagExtras();
        }
    }

    @JsonIgnore
    public String getFullTag() {
        return tag;
    }

    @JsonIgnore
    public String getTagExtras() {
        if (!tag.contains(TAG_DIVIDER)) {
            return "";
        }
        final String[] strings = tag.split("\\" + TAG_DIVIDER);
        if (strings.length > 1) {
            return strings[1];
        }
        return "";
    }

    @JsonIgnore
    public void setTagExtras(final String extras) {
        this.tag = getTag() + TAG_DIVIDER + extras;
    }

    @Override
    public int compareTo(final DimensionKey dimensionKey) {
        return hash.toString().compareTo(dimensionKey.hash.toString());
    }

    @Override
    public String toString() {
        return dimension + ":" + displayLabel;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DimensionKey)) {
            return false;
        }
        final DimensionKey that = (DimensionKey) obj;
        return learningSource == that.learningSource && Objects.equals(displayLabel, that.displayLabel) && Objects.equals(hash, that.hash)
            && Objects.equals(dimension, that.dimension) && Objects.equals(tag, that.tag) && Objects.equals(parentId, that.parentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(displayLabel, hash, dimension, tag, parentId, learningSource);
    }
}
