package com.zdeno.service.util;

import java.io.IOException;
import java.util.*;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zdeno.model.AimEvent;
import com.zdeno.model.AimIncident;
import com.zdeno.service.*;

@Stateless
public class IncidentDBMapper {

    @Inject
    private Logger logger;

    private final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public AimIncident transformIncidentToAimIncident(final Incident incident) {
        final AimIncident aimIncident = new AimIncident();
        if (incident.getId() == 0L){
            incident.setId(31 * (System.nanoTime() + incident.getCreationTime()));
        }
        aimIncident.setId(incident.getId());

        aimIncident.setName(incident.getName());
        aimIncident.setCloseTime(new Date(incident.getCloseTime()));
        aimIncident.setCreationTime(new Date(incident.getCreationTime()));
        aimIncident.setConfidence(incident.getConfidence());
        aimIncident.setLastUpdatedTime(new Date(incident.getLastUpdatedTime()));
        aimIncident.setOccurrenceFactor(incident.getOccurrenceFactor());
        aimIncident.setStatus(incident.getStatus().toString());
        aimIncident.setDataSources(getDataSourcesAsJsonStringFromSet(incident.getDataSources()));
        aimIncident.addEvents(transformEventWrapperListIntoAimEventSet(incident.getEvents()));
        aimIncident.setDimensionKeys(getDimensionKeysObjectFromSetAsString(incident.getDimensionKeys()));
        return aimIncident;
    }

    private List<AimEvent> transformEventWrapperListIntoAimEventSet(final List<EventWrapper> eventWrappers) {
        final List<AimEvent> eventsTransformed = new ArrayList<>();
        AimEvent aimEventTemporal;
        for (final EventWrapper eventWrapper : eventWrappers) {
            aimEventTemporal = transformEventWrapperIntoEventEntity(eventWrapper);
            eventsTransformed.add(aimEventTemporal);
        }
        return eventsTransformed;
    }

    private AimEvent transformEventWrapperIntoEventEntity(final EventWrapper eventWrapper) {
        final AimEvent aimEvent = new AimEvent();
        aimEvent.setId(eventWrapper.getId());
        aimEvent.setDisplayName(eventWrapper.getDisplayName());
        aimEvent.setSecondaryId(eventWrapper.getSecondaryId());
        aimEvent.setSourceDisplayName(eventWrapper.getSourceDisplayName());
        aimEvent.setTopology(eventWrapper.getTopology());
        aimEvent.setTimestamp(new Date(eventWrapper.getTimestamp()));
        if (eventWrapper.getRawEvent() instanceof Map) {
            aimEvent.setRawEvent(getRawEventAsJsonStringFromMap((Map<String, Object>) eventWrapper.getRawEvent()));
        }
        aimEvent.setStatus(eventWrapper.getStatus().toString());
        aimEvent.setDimensionKeys(getDimensionKeysFromMapAsJsonString(eventWrapper.getDimensionKeys()));
        if (eventWrapper.getIdentifier() == 0L) {
            eventWrapper.setIdentifier(31 * (System.nanoTime() + eventWrapper.getTimestamp()));
        }
        aimEvent.setIdentifier(eventWrapper.getIdentifier());
        return aimEvent;
    }

    private List<EventWrapper> getIncidentEventsFromIncidentEntity(final List<AimEvent> events) {
        final List<EventWrapper> result = new ArrayList<>();
        if (events != null) {
            for (final AimEvent event : events) {
                result.add(getEventWrapperFromEventEntity(event));
            }
        }
        return result;
    }

    private EventWrapper getEventWrapperFromEventEntity(final AimEvent event) {
        final EventWrapper eventWrapper = new EventWrapper();
        eventWrapper.setId(event.getId());
        eventWrapper.setDisplayName(event.getDisplayName());
        eventWrapper.setSecondaryId(event.getSecondaryId());
        eventWrapper.setSourceDisplayName(event.getSourceDisplayName());
        eventWrapper.setTopology(event.getTopology());
        if (event.getTimestamp() != null) {
            eventWrapper.setTimestamp(event.getTimestamp().getTime());
        }
        if (event.getStatus() != null) {
            eventWrapper.setStatus(Status.valueOf(event.getStatus()));
        }
        eventWrapper.setRawEvent(getStringRawEventAsMap(event.getRawEvent()));
        eventWrapper.setDimensionKeys(getDimensionKeysFormJsonStringAsMap(event.getDimensionKeys()));
        eventWrapper.setIdentifier(event.getIdentifier());
        return eventWrapper;
    }

    private Map<DimensionEnum, DimensionKey> getDimensionKeysFormJsonStringAsMap(final String dimensionKeysStr) {
        Map<DimensionEnum, DimensionKey> dimensionKeyEvents = new HashMap<>();
        try {
            if (dimensionKeysStr != null) {
                dimensionKeyEvents = mapper.readValue(dimensionKeysStr, new TypeReference<Map<DimensionEnum, DimensionKey>>() {
                });
            }
        } catch (IOException e) {
            logger.warn("DimensionKeys String {} cannot be mapped to Map, message {} ", dimensionKeysStr, e.getMessage());
            logger.warn(e.getMessage(), e);
        }
        return dimensionKeyEvents;
    }

    private Map<String, Object> getStringRawEventAsMap(final String rawEvent) {
        Map<String, Object> rawMap = new HashMap<>();
        try {
            if (rawEvent != null) {
                rawMap = mapper.readValue(rawEvent, new TypeReference<Map<String, Object>>() {
                });
            }
        } catch (IOException e) {
            logger.warn("Object rawEvent {} cannot be mapped, message {} ", rawEvent, e.getMessage());
        }
        return rawMap;
    }

    private String getDimensionKeysFromMapAsJsonString(final Map<DimensionEnum, DimensionKey> dimensionKeys) {
        String dimensionInJsonString = "";
        try {
            dimensionInJsonString = mapper.writeValueAsString(dimensionKeys);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
        return dimensionInJsonString;

    }

    private String getDimensionKeysObjectFromSetAsString(final Set<DimensionKey> dimensionKeys) {
        String dimensionInJsonString = "";
        try {
            dimensionInJsonString = mapper.writeValueAsString(dimensionKeys);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
        return dimensionInJsonString;
    }

    private Set<DimensionKey> getDimensionKeysInSetFormFromJsonString(final String dimensionKeysString) {
        Set<DimensionKey> dimensionKeys = new HashSet<>();
        try {
            dimensionKeys = mapper.readValue(dimensionKeysString, new TypeReference<Set<DimensionKey>>() {
            });
        } catch (IOException e) {
            logger.warn("Object dimensionKeys {} cannot be mapped, message {} ", dimensionKeysString, e.getMessage());
            logger.warn(e.getMessage(), e);
        }
        return dimensionKeys;
    }

    public Incident transformAimIncidentWithoutEventsToIncident(final Object[] objects) {
        final Incident incident = new Incident();

        incident.setId((Long) objects[0]);
        incident.setName((String) objects[1]);
        incident.setCreationTime(((java.sql.Timestamp) objects[2]).getTime());
        incident.setCloseTime(((java.sql.Timestamp) objects[3]).getTime());
        incident.setLastUpdatedTime(((java.sql.Timestamp) objects[4]).getTime());
        incident.setConfidence((Double) objects[5]);
        incident.setOccurrenceFactor((Double) objects[6]);
        final Set<String> dataSources = getDataSourcesFromJsonAsSet((String) objects[7]);
        for (final String ds : dataSources) {
            incident.addDataSource(ds);
        }
        incident.setStatus(IncidentStatusEnum.valueOf((String) objects[8]));
        incident.setDimensionKeys(getDimensionKeysInSetFormFromJsonString((String) objects[9]));
        return incident;
    }

    public Incident transformAimIncidentToIncident(final AimIncident aimIncident) {
        final Incident incident = new Incident();

        incident.setId(aimIncident.getId());
        incident.setName(aimIncident.getName());
        incident.setCreationTime(aimIncident.getCreationTime().getTime());
        incident.setCloseTime(aimIncident.getCloseTime().getTime());
        incident.setConfidence(aimIncident.getConfidence());
        incident.setLastUpdatedTime(aimIncident.getLastUpdatedTime().getTime());
        incident.setOccurrenceFactor(aimIncident.getOccurrenceFactor());
        incident.setStatus(IncidentStatusEnum.valueOf(aimIncident.getStatus()));
        incident.setDimensionKeys(getDimensionKeysInSetFormFromJsonString(aimIncident.getDimensionKeys()));
        final Set<String> dataSources = getDataSourcesFromJsonAsSet(aimIncident.getDataSources());
        for (final String ds : dataSources) {
            incident.addDataSource(ds);
        }
        incident.setEvents(getIncidentEventsFromIncidentEntity(aimIncident.getEvents()));

        return incident;
    }

    public String getDataSourcesAsJsonStringFromSet(final Set<String> dataSources) {
        String dataSource = "";
        try {
            dataSource = mapper.writeValueAsString(dataSources);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }

        return dataSource;
    }

    private Set<String> getDataSourcesFromJsonAsSet(final String dataSources) {
        Set<String> result = new HashSet<>();
        try {
            result = mapper.readValue(dataSources, new TypeReference<Set<String>>() {
            });
        } catch (IOException e) {
            logger.warn("Object dataources {} cannot be mapped, message {} ", dataSources, e.getMessage());
        }
        return result;
    }

    public String getRawEventAsJsonStringFromMap(final Map<String, Object> rawMap) {
        String rawStr = "";
        try {
            rawStr = mapper.writeValueAsString(rawMap);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
        return rawStr;
    }

    /**
     * Update DB generated IDs to events from the given incident. <br>
     * <br>
     * <b>NOTE:</b> AimIncident and Incident must have the same events, if you cannot guarantee that these events are no longer the same, don't use
     * this method. Example: If you update database with Incident but in the meantime you suspect that someone is removing events from the same
     * incident, this method will fail. Adding of events is permitted but reshuffling or removing events is prohibited!!
     *
     * @param aimIncident
     *            - true incident from DB
     * @param incident
     *            - Incident object whose events should be updated.
     */
    public void syncEventIdentifiers(final AimIncident aimIncident, final Incident incident) {
        final Iterator<AimEvent> aimEventIterator = aimIncident.getEvents().iterator();
        final Iterator<EventWrapper> incidentIterator = incident.getEvents().iterator();
        while (aimEventIterator.hasNext()) {
            final AimEvent aimEvent = aimEventIterator.next();
            final EventWrapper incidentEvent = incidentIterator.next();
            incidentEvent.setIdentifier(aimEvent.getIdentifier());
        }
    }

    /**
     * Find in the AimIncident Events the id that match to the Incidents EventWrappers
     *
     * @param aimIncident
     * @param incident
     */
    public void setEventIdToWrapperEventsIdentifier(final AimIncident aimIncident, final Incident incident) {
        for (final EventWrapper eventWrapper : incident.getEvents()) {

            if (eventWrapper.getIdentifier() == 0) {
                eventWrapper.setIdentifier(getWrapperEventIdentifierIdFromAimIncidentEvents(eventWrapper, aimIncident));
            }
        }
    }

    private long getWrapperEventIdentifierIdFromAimIncidentEvents(final EventWrapper eventWrapper, final AimIncident aimIncident) {
        for (final AimEvent aimEvent : aimIncident.getEvents()) {
            if (eventWrapperIsEqualToAimEvent(eventWrapper, aimEvent)) {
                return aimEvent.getIdentifier();
            }
        }
        return 0;
    }

    private boolean eventWrapperIsEqualToAimEvent(final EventWrapper eventWrapper, final AimEvent aimEvent) {
        return eventWrapper.getDisplayName().equals(aimEvent.getDisplayName()) && eventWrapper.getTimestamp() == aimEvent.getTimestamp().getTime()
            && eventWrapper.getSecondaryId().equals(aimEvent.getSecondaryId());
    }
}
