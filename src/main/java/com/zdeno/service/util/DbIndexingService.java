package com.zdeno.service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;

/**
 * Bean tasked with creating indices for tables.
 */
@Singleton
@Startup
public class DbIndexingService {

    @Resource
    private TimerService timerService;

    @Inject
    private Logger logger;

    @PersistenceContext(unitName = "aimManager")
    private EntityManager entityManager;

    private static class Index{
        String tableName;
        String indexName;
        String columnsName; //comma separated if more than one

        Index(final String tableName, final String indexName, final String columnsName) {
            this.tableName = tableName;
            this.indexName = indexName;
            this.columnsName = columnsName;
        }
    }

    private static final List<Index> INDICES = new ArrayList<>(Arrays.asList(
        new Index("incident_aim", "idx_status", "status"),
        new Index("event", "fk_incidentIdent", "incident_id")));

    @PostConstruct
    public void postConstruct() {
        final TimerConfig timerConfig = new TimerConfig("IndexCreationBean", false);
        timerService.createSingleActionTimer(500, timerConfig);
    }

    /**
     * Creates indexes from INDICES list.
     */
    @Timeout
    private void timeout() {
        for (final Index index : INDICES) {
            try {
                createIndexOn(index.indexName, index.tableName, index.columnsName);
            } catch (final Exception e) {
                logger.warn("Exception while trying to create index {} on tables {}. Exception: {}", index.indexName, index.tableName,
                        index.columnsName, e.getMessage());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void createIndexOn(final String indexName, final String tableName, final String tableColumns) {
        logger.info("Creating index {} on table {} for column {}", indexName, tableName, tableColumns);
        final List<String> resultList = (List<String>) entityManager.createNativeQuery(
            "SELECT\n"
            + "    c.relname AS index_name\n"
            + "FROM\n"
            + "    pg_class AS a\n"
            + "    JOIN pg_index AS b ON (a.oid = b.indrelid)\n"
            + "    JOIN pg_class AS c ON (c.oid = b.indexrelid)\n"
            + "WHERE\n"
            + "    a.relname = '"+tableName+"';").getResultList();
        for (final String index : resultList) {
            if (indexName.trim().equalsIgnoreCase(index.trim())) {
                logger.info("Index {} on table {} already exists", indexName, tableName);
                return;
            }
        }
        entityManager.createNativeQuery("create index " + indexName + " on " + tableName + "(" + tableColumns + ");").executeUpdate();
        logger.info("Index {} on table {} for column {} was created successfully.", indexName, tableName, tableColumns);
    }
}
