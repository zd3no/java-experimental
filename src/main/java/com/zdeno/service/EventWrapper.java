package com.zdeno.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Provides a generic base class as a wrapper for all types of events that pass through the system so that they can have common functionality such as
 * retrieving their ID or topology information.
 */
public class EventWrapper<T> implements Serializable {

    private static final long serialVersionUID = 2129096133125295994L;

    @Transient
    private long identifier;
    private Map<DimensionEnum, DimensionKey> dimensionKeys = new HashMap<>();
    private T rawEvent;
    private String id;
    private String displayName;
    private String sourceDisplayName = "SOURCE_UNDEFINED";
    private String topology;
    private Status status = Status.NONE;
    private String secondaryId;
    private long timestamp = 0;

    @Transient
    @JsonIgnore
    private boolean shouldBeClosedAfterNesting;

    public Map<DimensionEnum, DimensionKey> getDimensionKeys() {
        return dimensionKeys;
    }

    public void setDimensionKeys(final Map<DimensionEnum, DimensionKey> dimensionKeys) {
        this.dimensionKeys = dimensionKeys;
    }

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final long identifier) {
        this.identifier = identifier;
    }

    /**
     * @return The raw event that is wrapped.
     */
    public T getRawEvent() {
        return rawEvent;
    }

    /**
     * @param rawEvent
     *            The raw event that is wrapped.
     */
    public void setRawEvent(final T rawEvent) {
        this.rawEvent = rawEvent;
    }

    /**
     * Gets the raw event type. Example: FMAlarm, KPIAnomaly.
     *
     * @return event (feed) type or "UNKNOWN TYPE" if raw event is either null or doesn't contain "type" attribute. Can return null if "type"
     *         attribute value in raw event is null.
     */
    public String getType() {
        if (getRawEvent() instanceof Map) {
            final Object res = ((Map) getRawEvent()).get("type");
            if (res != null) {
                return res.toString();
            } else {
                return "UNKNOWN TYPE";
            }
        } else if (getRawEvent() == null) {
            return "UNKNOWN TYPE";
        }
        return getRawEvent().getClass().getSimpleName();
    }

    /**
     * @return id - Might not actually be unique if the "same" event is nested in multiple incidents.
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            - Might not actually be unique if the "same" event is nested in multiple incidents.
     */
    public void setId(final String id) {
        this.id = id;
    }

    public String getSecondaryId() {
        return secondaryId;
    }

    public void setSecondaryId(final String secondaryId) {
        this.secondaryId = secondaryId;
    }

    public void addDimensionKey(final DimensionEnum dimension, final DimensionKey key) {
        dimensionKeys.put(dimension, key);
    }

    public DimensionKey getDimensionKey(final DimensionEnum dimension) {
        return dimensionKeys.get(dimension);
    }

    public Collection<DimensionKey> getAllDimensionKeys() {
        return dimensionKeys.values();
    }

    public Collection<DimensionEnum> getAllDimensionTypes() {
        return dimensionKeys.keySet();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return A user friendly display name for the event itself. could be based on anything...
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName
     *            A user friendly display name for the event itself. could be based on anything...
     */
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return A user friendly display name for the source for this event like FMAlarm, Configuration Change, Command
     */
    public String getSourceDisplayName() {
        return sourceDisplayName;
    }

    /**
     * @param sourceDisplayName
     *            A user friendly display name for the source for this event like FMAlarm, Configuration Change, Command
     */
    public void setSourceDisplayName(final String sourceDisplayName) {
        this.sourceDisplayName = sourceDisplayName;
    }

    /**
     * @return Where this event is coming from to targeted at. note this could be a Network node or cell but also a geo location or function type
     *         depending on the feed
     */
    public String getTopology() {
        return topology;
    }

    /**
     * @param topology
     *            Where this event is coming from to targeted at. note this could be a Network node or cell but also a geo location or function type
     *            depending on the feed
     */
    public void setTopology(final String topology) {
        this.topology = topology;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public boolean isShouldBeClosedAfterNesting() {
        return shouldBeClosedAfterNesting;
    }

    public void setShouldBeClosedAfterNesting(final boolean shouldBeClosedAfterNesting) {
        this.shouldBeClosedAfterNesting = shouldBeClosedAfterNesting;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final EventWrapper<?> that = (EventWrapper<?>) obj;

        return timestamp == that.timestamp
                && (dimensionKeys != null ? dimensionKeys.equals(that.dimensionKeys)
                        : that.dimensionKeys == null
                                && (rawEvent != null ? rawEvent.equals(that.rawEvent)
                                        : that.rawEvent == null && (id != null ? id.equals(that.id)
                                                : that.id == null && (displayName != null ? displayName.equals(that.displayName)
                                                        : that.displayName == null && (sourceDisplayName != null
                                                                ? sourceDisplayName.equals(that.sourceDisplayName)
                                                                : that.sourceDisplayName == null && (topology != null ? topology.equals(that.topology)
                                                                        : that.topology == null && status == that.status
                                                                                && (status != null ? status.equals(that.status)
                                                                                        : that.status == null && (secondaryId != null
                                                                                                ? secondaryId.equals(that.secondaryId)
                                                                                                : that.secondaryId == null))))))));
    }

    @Override
    public int hashCode() {
        int result = dimensionKeys != null ? dimensionKeys.hashCode() : 0;
        result = 31 * result + (rawEvent != null ? rawEvent.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        result = 31 * result + (sourceDisplayName != null ? sourceDisplayName.hashCode() : 0);
        result = 31 * result + (topology != null ? topology.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (secondaryId != null ? secondaryId.hashCode() : 0);
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("EventWrapper{");
        stringBuilder.append("dimensionKeys=").append(dimensionKeys);
        stringBuilder.append(", rawEvent=").append(rawEvent);
        stringBuilder.append(", id='").append(id).append('\'');
        stringBuilder.append(", displayName='").append(displayName).append('\'');
        stringBuilder.append(", sourceDisplayName='").append(sourceDisplayName).append('\'');
        stringBuilder.append(", topology='").append(topology).append('\'');
        stringBuilder.append(", status=").append(status);
        stringBuilder.append(", secondaryId='").append(secondaryId).append('\'');
        stringBuilder.append(", timestamp=").append(timestamp);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
