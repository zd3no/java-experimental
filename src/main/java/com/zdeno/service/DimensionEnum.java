package com.zdeno.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Dimension enum is a String that used to be an enum signalizing the type of the dimensionKey.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DimensionEnum implements Comparable<Object>, Serializable {

    private static final long serialVersionUID = 4756173648191900570L;
    private static Map<String, DimensionEnum> createdDimensions = new HashMap();

    //String reservedDimensions = "TOPOLOGY, PROBLEM_CAUSE, SPECIFIC_PROBLEM, CHANGED_ATTRIBUTE, COMBINED, WHITELIST, BLACKLIST, COORDINATES,
    // WEATHER_REPORT, CM_TOPOLOGY";
    String dimension = "NONE";

    /**
     * Convert Strings to DimensionEnum and return as set.
     *
     * @param strings - Strings to be converted as DimensionEnum
     * @return set of DimensionEnum
     */
    public static Set<DimensionEnum> toDimensionEnumSet(final Iterable<String> strings) {
        final Set<DimensionEnum> set = new HashSet<>();
        for (final String string : strings) {
            set.add(new DimensionEnum(string));
        }
        return set;
    }

    public static DimensionEnum valueOf(final String d) {
        if (d == null) {
            return null;
        }
        if (createdDimensions.containsKey(d)) {
            return createdDimensions.get(d);
        } else {
            createdDimensions.put(d, new DimensionEnum(d));
            return createdDimensions.get(d);
        }
    }

    @Deprecated
    public static DimensionEnum getValueOf(final String d) {
        return valueOf(d);
    }

    public static boolean exists(final String dimension) {
        return createdDimensions.containsKey(dimension);
    }

    public DimensionEnum() { }

    public DimensionEnum(final String d) {
        dimension = d;
    }

    public void setDimension(final String d) {
        dimension = d;
    }

    public String getDimension() {
        return dimension;
    }

    @JsonIgnore
    @Deprecated
    public boolean isCombinedDimension() {
        return dimension.contains(":");
    }

    @Override
    public int compareTo(final Object o) {
        return dimension.compareTo(o.toString());
    }

    @Override
    public String toString() {
        return dimension;
    }

    @Override
    public int hashCode() {
        return dimension.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof DimensionEnum)) {
            return false;
        }
        return dimension.compareTo(((DimensionEnum) other).dimension) == 0;
    }

}
