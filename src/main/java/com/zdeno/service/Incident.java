package com.zdeno.service;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.persistence.Transient;

/**
 * Represents a group of events that have been grouped together in an incident.
 */
public class Incident implements Serializable {
    private List<EventWrapper> events = new CopyOnWriteArrayList<>();
    private Set<DimensionKey> dimensionKeys = new ConcurrentSkipListSet<>();
    private long id;
    private String name;
    private IncidentStatusEnum status = IncidentStatusEnum.OPEN;
    private long creationTime;
    private long closeTime;
    private long lastUpdatedTime;
    private double confidence;
    private double occurrenceFactor;
    private final Set<String> dataSources = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());

    @Transient
    private double priority;
    private Map<String, Long> serviceImpactIndicators = new HashMap<>();

    public Incident() {
    }

    public Incident(final Incident incidentToCopy) {
        super();
        events = new CopyOnWriteArrayList<>(incidentToCopy.getEvents());
        dimensionKeys = new ConcurrentSkipListSet<>(incidentToCopy.getDimensionKeys());
        id = incidentToCopy.getId();
        name = incidentToCopy.getName();
        status = incidentToCopy.getStatus();
        creationTime = incidentToCopy.getCreationTime();
        lastUpdatedTime = incidentToCopy.getLastUpdatedTime();
        confidence = incidentToCopy.getConfidence();
        occurrenceFactor = incidentToCopy.getOccurrenceFactor();
        dataSources.addAll(incidentToCopy.getDataSources());
        closeTime = incidentToCopy.getCloseTime();
    }

    @SuppressWarnings("unchecked")
    private Set<DimensionKey> generateDimensionKeys(final List<EventWrapper> events) {
        final Set<DimensionKey> dimensionKeys = new HashSet<>();
        for (final EventWrapper eventWrapper : events) {
            dimensionKeys.addAll(eventWrapper.getAllDimensionKeys());
        }
        return dimensionKeys;
    }

    public List<EventWrapper> getEvents() {
        return Collections.unmodifiableList(events);
    }

    public void setEvents(final List<EventWrapper> events) {
        this.events = events;
        this.dimensionKeys = new ConcurrentSkipListSet<>(generateDimensionKeys(this.events));
    }

    public void addEventsToIncident(final List<EventWrapper> events) {
        this.events.addAll(events);
        dimensionKeys = new ConcurrentSkipListSet<>(generateDimensionKeys(this.events));
    }

    public void setId(final long incidentId) {
        this.id = incidentId;
    }

    public long getId() {
        return id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setServiceImpactIndicators(final Map<String, Long> serviceImpactIndicators) {
        this.serviceImpactIndicators = serviceImpactIndicators;
    }

    public Long getServiceImpactIndicator(final String kpi) {
        if (!serviceImpactIndicators.containsKey(kpi)) {
            return 0L;
        }
        if (getStatus() == IncidentStatusEnum.CLOSED) { //service impact of closed incidents is 0
            return 0L;
        }
        return serviceImpactIndicators.get(kpi);
    }

    public Set<DimensionKey> getDimensionKeys() {
        return dimensionKeys;
    }

    public void setDimensionKeys(final Set<DimensionKey> dimensionKeys) {
        this.dimensionKeys = new ConcurrentSkipListSet<>(dimensionKeys);
    }

    public IncidentStatusEnum getStatus() {
        return status;
    }

    public void setStatus(final IncidentStatusEnum status) {
        this.status = status;
    }

    public Set<String> getDataSources() {
        return dataSources;
    }

    public void addDataSource(final String dataSource) {
        dataSources.add(dataSource);
    }

    public void addDataSources(final Collection<String> dataSources) {
        this.dataSources.addAll(dataSources);
    }

    public void setConfidence(final double confidence) {
        this.confidence = confidence;
    }

    public double getConfidence() {
        return confidence;
    }

    public double getOccurrenceFactor() {
        return occurrenceFactor;
    }

    public void setOccurrenceFactor(final double occurrenceFactor) {
        this.occurrenceFactor = occurrenceFactor;
    }

    public long getLastUpdatedTime() {
        if (lastUpdatedTime > 0) {
            return lastUpdatedTime;
        } else {
            return getCreationTime();
        }
    }

    public void setLastUpdatedTime(final long lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public void setCreationTime(final long creationTime) {
        this.creationTime = creationTime;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public long getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(final long closeTime) {
        this.closeTime = closeTime;
    }

    public void updateTime() {
        this.lastUpdatedTime = new Date().getTime();
    }

    public boolean isOpen() {
        return status.equals(IncidentStatusEnum.OPEN);
    }

    public void setPriority(final double priority) {
        this.priority = priority;
    }

    public double getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("Incident=").append(this.hashCode()).append("{");
        stringBuilder.append(",name=").append(name);
        stringBuilder.append(",dimensionKeys=").append(dimensionKeys);
        stringBuilder.append(",status=").append(status);
        stringBuilder.append(",creationTime=").append(creationTime);
        stringBuilder.append(",dataSources=").append(dataSources);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = events != null ? events.hashCode() : 0;
        temp = Double.doubleToLongBits(confidence);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        final Incident incident = (Incident) object;

        if (incident.getId() != id) {
            return false;
        }

        if (Double.compare(incident.confidence, confidence) != 0) {
            return false;
        }

        return events != null ? events.equals(incident.events) : incident.events == null;
    }
}
