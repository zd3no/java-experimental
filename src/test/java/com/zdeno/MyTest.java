package com.zdeno;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

import com.zdeno.experimental.properties.PrettyProperties;

public class MyTest {


    @Test
    public void writeToPropertiesFile() throws IOException {
        new PrettyProperties().postConstruct();
        final Properties properties = new Properties();
        properties.load(new FileInputStream("/tmp/my.properties"));
        System.out.println(properties);

        Assert.assertTrue(properties.size() == 2);
    }

}
